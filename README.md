# Users application back-end

## Requirements

install Java 1.8 or higher

install tomcat 8.x or higher

install mysql 5.7.17 or higher

## Build & development

To start a project use:
`git clone https://github.com/valerik931/user-app.git`

`cd user-app`

`mvn clean install`

`java -jar target/dependency/webapp-runner.jar target/*.war`

project will be available on http://localhost:8080/

## CheckStyle plugin installation

Open `file>Settings>IDE Settings>plugins`. 
Click Browse repositories…
Find «Checkstyle-idea» and install plugin.

## Preview

#### Back-end

https://team-user-app.herokuapp.com/api

#### Front-end

https://serene-wave-27221.herokuapp.com/#/contacts

## Specification

https://docs.google.com/document/d/1YB5Ow1lHN82gWVCLLsBPHrFJ67o5Oh-8laBbWgSic2c/